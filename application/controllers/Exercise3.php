<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exercise3 extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('router_details');
  }

  public function index() {
    echo 'Coming Soon';
  }

  public function add_new_router($authentication = ''){
    $err_flag = 0;
    $err_msg = array();

    $posted_data = $this->input->post(NULL, true);
    extract($posted_data);

    if(!isset($sapid) || empty($sapid)){
      $err_flag = 1;
      $err_msg['sapid'] = 'SAP ID field is required';
    }
    elseif(!$this->unique_fieldcheck($sapid, 0)){
      $err_flag = 1;
      $err_msg['sapid'] = 'SAP ID details already exists';
    }

    if(!isset($hostname) || empty($hostname)){
      $err_flag = 1;
      $err_msg['hostname'] = 'Hostname field is required';
    }

    if(!isset($loopback) || empty($loopback)){
      $err_flag = 1;
      $err_msg['loopback'] = 'Loopback (ipv4) field is required';
    }

    if(!isset($mac_address) || empty($mac_address)){
      $err_flag = 1;
      $err_msg['mac_address'] = 'MAC address field is required';
    }

    $output_arr['message'] = '';
    if($err_flag == 0){
      $model_result = $this->router_details->add_edit_router_details(array('data' => array('sapid' => $sapid,
                            'hostname' => $hostname,
                            'loopback' => $loopback,
                            'mac_address' => $mac_address,
                            'created' => date('Y-m-d H:i:s'),
                            'id' => ((isset($record_id) && !empty($record_id)) ? $record_id : 0)
                          )
                        )
                      );
      if($model_result){
        $output_arr['message'] = 'Router details added successfully';
      }
      else{
        $err_flag = 1;
        $err_msg['sapid'] = 'Unable to add router details, try again';
      }
    }

    $output_arr['err_flag'] = $err_flag;
    $output_arr['err_msg'] = $err_msg;
    echo json_encode($output_arr);
  }

  public function unique_fieldcheck($str, $id) {
    $txt_RegID = (int) $id;
    $knownRecordCount = $this->db->where(array('id !=' => $txt_RegID, 'sapid' => $str))->get('router_details')->num_rows();
    $knownRecordCount = (int) $knownRecordCount;
    if( $knownRecordCount > 0 ) {
      return false;
    }
    else {
      return true;
    }
  }
}

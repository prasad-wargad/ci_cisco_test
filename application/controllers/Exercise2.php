<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exercise2 extends CI_Controller {
  public function __construct() {
    parent::__construct();
  }

  public function index() {
    echo 'Coming Soon';
  }

  public function question_1(){
    $current_working_dir = getcwd();
    $total_disk_space = disk_total_space($current_working_dir);
    $free_disk_space = disk_free_space($current_working_dir);
    $disk_space_used = $total_disk_space - $free_disk_space;

    echo '<br>Question c)<br> *************************************************************** <br>';
    echo '<br>Total Disk Usage='. $this->convertToReadableSize($total_disk_space) .'<br>';
    echo '<br>Free Disk Usage='. $this->convertToReadableSize($free_disk_space) .'<br>';
    echo '<br>Disk Usage='. $this->convertToReadableSize($disk_space_used) .'<br>';
    echo '<br> *************************************************************** <br>';

    echo '<br>Question d)<br> *************************************************************** <br>';
    echo '<br>Inode Usage=<br>';
    exec("df -i", $output_var, $return_var);
    echo print_r($output_var, true);
    echo '<br> *************************************************************** <br>';

    echo '<br>Question e)<br> *************************************************************** <br>';
    echo '<br>Get list of files from current working directory=';
    exec("ls", $output_var, $return_var);
    echo print_r($output_var, true);
    echo '<br> *************************************************************** <br>';
  }

  public function question_3() {
    echo "<br>Below is the command to restart the APACHE server, in case if load is too much:<br>";
    echo "/etc/init.d/apache2 restart";
  }

  public function question_4() {
    echo "<br>Server Peformance related commands:";
    echo "<br>a) command to observe the load or troubleshoot: <b>uptime, w</b>";
    echo "<br>b) command to identify which process are consuming more load on the server: <b>top</b>";
    echo "<br>b) command to minimize the server load: <b>can restart the APACHE</b> or <b>can kill processes which are consuming more CPU resources</b>";
  }

  public function convertToReadableSize($size){
    $base = log($size) / log(1024);
    $suffix = array("", "KB", "MB", "GB", "TB");
    $f_base = floor($base);
    return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
  }
}

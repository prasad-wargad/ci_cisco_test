<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Exercise1 extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('router_details');
  }

  public function index() {
    $data = array('table_fields' => array('sapid' => array('label' => 'SAP ID'),
                                          'hostname' => array('label' => 'HostName'),
                                          'loopback' => array('label' => 'Loopback (IPV4)'),
                                          'mac_address' => array('label' => 'MAC Address'),
                                          'status' => array('label' => 'Record Status'),
                                          'created' => array('label' => 'Created Date'),
                                          'action' => array('label' => 'Action')));
    $this->load->view('exercise1/data_view', $data);
  }

  public function addedit($record_id = 0){
    $posted_data = $this->input->post(NULL, true);
    extract($posted_data);

    $existing_data = array();
    $data = array();
    if(isset($record_id) && !empty($record_id) && is_numeric($record_id)){
      $existing_data = $this->router_details->get_router_details(array('record_id' => $record_id));
      if(isset($existing_data['data'][0]) && is_array($existing_data['data'][0]) && count($existing_data['data'][0]) > 0){
        $data = $existing_data['data'][0];
        $data['record_status'] = $data['status'];
      }
      unset($existing_data);
    }

    if(isset($btn_submit) && !empty($btn_submit)){
      $data = $posted_data;
      if(!isset($data['record_status']) || empty($data['record_status']) || !is_numeric($data['record_status'])){
        $data['record_status'] = 0;
      }

      $this->form_validation->set_rules('sapid', 'SAP ID', 'required|min_length[3]|max_length[18]|callback_unique_fieldcheck['. $record_id .']');
      $this->form_validation->set_rules('hostname', 'hostname', 'required');
      $this->form_validation->set_rules('loopback', 'loopback (ipv4)', 'required');
      $this->form_validation->set_rules('mac_address', 'MAC address', 'required');

      if(!isset($record_status) || empty($record_status) || !is_numeric($record_status)){
        $record_status = 0;
      }

      if($this->form_validation->run()) {
        $model_result = $this->router_details->add_edit_router_details(array('data' => array('sapid' => $sapid,
                              'hostname' => $hostname,
                              'loopback' => $loopback,
                              'mac_address' => $mac_address,
                              'status' => $record_status,
                              'created' => date('Y-m-d H:i:s'),
                              'id' => ((isset($record_id) && !empty($record_id)) ? $record_id : 0)
                            )
                          )
                        );
        if($model_result) {
          $this->session->set_flashdata('msg', 'Router details saved successfully.');
        }
        header('Location: '. base_url() .'index.php/exercise1');
        exit;
      }
      else{
        if(form_error('sapid') !== null && form_error('sapid') != '') {
          $data['sapid_error'] = form_error('sapid');
        }
        if(form_error('hostname') !== null && form_error('hostname') != '') {
          $data['hostname_error'] = form_error('hostname');
        }
        if(form_error('loopback') !== null && form_error('loopback') != '') {
          $data['loopback_error'] = form_error('loopback');
        }
        if(form_error('mac_address') !== null && form_error('mac_address') != '') {
          $data['mac_address_error'] = form_error('mac_address');
        }
      }
    }
    $this->load->view('exercise1/addedit_view', $data);
  }

  public function router_list() {
    $posted_data = $this->input->post(NULL, true);
    $output_arr = $this->router_details->router_list($posted_data);
    echo json_encode($output_arr);
  }

  public function question_3() {
    // data generator script
    echo PHP_EOL .'Please enter number of records: ';
    $stdin = fopen('php://stdin', 'r');
    $no_of_records = trim(strip_tags(fgets($stdin)));
    fclose($stdin);
    if(isset($no_of_records) && !empty($no_of_records) && is_numeric($no_of_records)){
      if($no_of_records > 100){
        echo PHP_EOL .'Oops, you can not enter generate more than 100 records';
      }
      else {
        for($cntr = 1; $cntr <= $no_of_records; $cntr++){
          $sapid = "sapid_". uniqid();
          $hostname = "hostname_". uniqid();
          $loopback = "loopback_". uniqid();
          $mac_address = "mac_address_". uniqid();
          $record_status = 1;
          $record_id = 0;
          $model_result = $this->router_details->add_edit_router_details(array('data' => array('sapid' => $sapid,
                                'hostname' => $hostname,
                                'loopback' => $loopback,
                                'mac_address' => $mac_address,
                                'status' => $record_status,
                                'created' => date('Y-m-d H:i:s'),
                                'id' => ((isset($record_id) && !empty($record_id)) ? $record_id : 0)
                              )
                            )
                          );
        }
        unset($cntr);
      }
    }
    else{
      echo PHP_EOL .'You have entered an incorrect value, try again';
    }
  }

  public function question_4() {
    // draw geometric figures
    header("Content-type: image/png");
     
    $img_width = 300;
    $img_height = 300;
     
    $img = imagecreatetruecolor($img_width, $img_height);
    
    $white = imagecolorallocate($img, 255, 255, 255);
    $red   = imagecolorallocate($img, 255, 0, 0);
    
    imagefill($img, 0, 0, $white);
    imagepolygon($img, [100, 100, 200, 100, 100, 100, 85, 150, 85, 150, 100, 200, 100, 200, 200, 200, 200, 200, 215, 150, 215, 150, 200, 100], 12, $red);

    imageellipse($img, 150, 150, 90, 90, $red);

    imageline($img, 150, 120, 120, 150, $red);
    imageline($img, 120, 150, 150, 180, $red);
    imageline($img, 150, 180, 180, 150, $red);
    imageline($img, 180, 150, 150, 120, $red);

    imagepng($img);
  }

  public function unique_fieldcheck($str, $id) {
    $txt_RegID = (int) $id;
    $knownRecordCount = $this->db->where(array('id !=' => $txt_RegID, 'sapid' => $str))->get('router_details')->num_rows();
    $knownRecordCount = (int) $knownRecordCount;
    if( $knownRecordCount > 0 ) {
      $this->form_validation->set_message('unique_fieldcheck', 'SAP ID details already exists');
      return false;
    }
    else {
      return true;
    }
  }

  public function soft_delete_record(){
    $posted_data = $this->input->post(NULL, true);
    extract($posted_data);

    $err_flag = 0;
    $err_msg = array();

    $output_arr['message'] = 'Unable to process your request, try again later';

    if(!isset($record_id) || empty($record_id) || !is_numeric($record_id)){
      $err_flag = 1;
      $err_msg[] = 'Record details not found';
    }

    if($err_flag == 0){
      $model_result = $this->router_details->add_edit_router_details(array('data' => array('status' => 0,
                            'id' => ((isset($record_id) && !empty($record_id)) ? $record_id : 0)
                          )
                        )
                      );
      if($model_result){
        $output_arr['message'] = 'Record deleted successfully';
      }
    }
    $output_arr['err_flag'] = $err_flag;
    $output_arr['err_msg'] = $err_msg;
    echo json_encode($output_arr);
  }
}

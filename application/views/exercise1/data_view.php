<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Router Details Page</title>
    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
  </head>
  <body>
    <header>
      <div class="collapse bg-dark" id="navbarHeader"></div>
      <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
          <a href="javascript:void(0);" class="navbar-brand d-flex align-items-center">
            <strong>Router Details Page</strong>
          </a>
        </div>
      </div>
    </header>

    <div class="container">
      <div class="row">
        <div class="col-md-12">&nbsp;</div> <!--/.col-md-12-->
      </div> <!--/.row-->
        <?php
          if( $this->session->flashdata('msg') !== null && $this->session->flashdata('msg') != '' ) {
            echo '
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">&nbsp;</div>
              </div>
              <!-- /.row -->
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    '. $this->session->flashdata('msg') .'
                  </div>
                </div>
                <!-- /.col-md-12 -->
              </div>
              <!-- /.row -->
              ';
          }
        ?>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-right">
          <button type="button" class="btn btn-info" id="btn_add_new">Add New</button>
        </div> <!--/.col-md-12 -->
      </div> <!--/.row-->
      <div class="row">
        <div class="col-md-12">&nbsp;</div> <!--/.col-md-12-->
      </div> <!--/.row-->
      <div class="row">
        <div class="col-md-12">
          <?php
            $table_headers = '<tr>';
            foreach($table_fields as $key => $value){
              $table_headers .= '<th data-column='. $key .'>'. $value['label'] .'</th>';
            }
            $table_headers .= '</tr>';
            unset($key, $value);
          ?>
          <table id="tbl_details" class="display" width="100%">
            <thead>
              <?php
                echo $table_headers;
                echo $table_headers;
              ?>
            </thead>
          </table> <!--/.#tbl_details-->
        </div> <!--/.col-md-12-->
      </div> <!--/.row-->
    </div> <!--/.container-->

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
      </div>
    </footer>
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.5/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
      var tableDataTable;
      function is_edit(record_id){
        if(record_id != null && typeof record_id != "undefined" && $.isNumeric(record_id)){
          window.open("<?php echo base_url(); ?>index.php/exercise1/addedit/"+ record_id, "_parent");
        }
      }

      function is_delete(record_id){
        if(record_id != null && typeof record_id != "undefined" && $.isNumeric(record_id)){
          $.ajax({
            "url": "<?php echo base_url(); ?>index.php/exercise1/soft_delete_record",
            "dataType": "json",
            "type": "POST",
            "data": {"record_id":record_id},
            "error": function(){
              alert("Error processing your request, try again later");
            },
            "success": function(response){
              if(response.err_flag == 1 && response.err_msg != null){
                var err_msg = "";
                $.each(response.err_msg, function(key, value){
                  err_msg += value;
                });
                alert(err_msg);
              }
              else{
                alert(response.message);
              }
              tableDataTable.draw();
            }
          });
        }
      }

      $(document).ready(function(){
        $("#btn_add_new").on("click", function(){
          window.open("<?php echo base_url(); ?>index.php/exercise1/addedit", "_parent");
        });

        var txtColObj = [];
        $("#tbl_details thead tr:first").find("th").each(function(){
          var data_column = $(this).attr("data-column"), txtSearchFieldHTML = '';
          txtColObj.push({"data": data_column});
          switch(data_column){
            case 'status':
              var options = {'1':'Active', '0':'Deleted'};

              txtSearchFieldHTML  = '<select class="form-control" data-column="'+ data_column +'">';
                txtSearchFieldHTML += '<option value="">All</option>';
                $.each(options, function(key, value){
                  txtSearchFieldHTML += '<option value="'+ key +'">'+ value +'</option>';
                });
              txtSearchFieldHTML += '</select>';
              break;
            case 'created':
              txtSearchFieldHTML += '<div class="row">'+
                                      '<div class="col-xs-4"><input type="date" data-column="'+ data_column +'" class="form-control from_date"></div>'+
                                        '<div class="col-xs-4">&nbsp;-&nbsp;</div>'+
                                        '<div class="col-xs-4"><input type="date" data-column="'+ data_column +'" class="form-control to_date"></div>'+
                                    '</div>';
              break;
            case 'action':
              break;
            default:
              txtSearchFieldHTML = '<input type="text" data-column="'+ data_column +'" class="form-control" placeholder="Search '+ $(this).text() +'">';
          }
          $(this).html(txtSearchFieldHTML);
        });

        tableDataTable = $('#tbl_details').DataTable({
          "processing": true,
          "serverSide": true,
          "ajax": {
            "url": "<?php echo base_url(); ?>index.php/exercise1/router_list",
            "type": "POST",
          },
          "columns": txtColObj,
          "columnDefs": [
            {"sortable": false, "targets": 6, "width": "10%"}
          ],
          "searching": true
        });

        $("div#tbl_details_wrapper").find("div#tbl_details_filter").remove();

        // Apply the filter
        tableDataTable.columns().indexes().each( function (idx) {
          var txtObjType = ["input", "select"];
          for(var txtCntr = 0; txtCntr < txtObjType.length; txtCntr++) {
            strObjType = txtObjType[txtCntr];
            $("table.dataTable thead > tr > th").eq(idx).find(strObjType).on("change", function(){
              var txtSearchedValue=this.value;
              if($(this).attr("data-column") !== null && $(this).attr("data-column") == "created"){
                txtSearchedValue  = $(this).closest("div.row").find('input[type="date"].from_date').val();
                txtSearchedValue += ',' + $(this).closest("div.row").find('input[type="date"].to_date').val();
              }
              tableDataTable
                .column(idx)
                .search(txtSearchedValue)
                .draw();
            });
          }
        });
      });
    </script>
  </body>
</html>

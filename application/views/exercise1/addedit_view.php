<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Router Details Page</title>
    <!-- Bootstrap core CSS -->
    <link href="https://getbootstrap.com/docs/4.5/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
  </head>
  <body>
    <header>
      <div class="collapse bg-dark" id="navbarHeader"></div>
      <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
          <a href="javascript:void(0);" class="navbar-brand d-flex align-items-center"> 
            <strong>Router Add/Edit Page</strong>
          </a>
        </div>
      </div>
    </header>

    <div class="container">
      <div class="row">
        <div class="col-md-12">&nbsp;</div> <!--/.col-md-12-->
      </div> <!--/.row-->
      <div class="row">
        <div class="col-md-12">
          <form action="" method="post" autocomplete="off">
            <div class="form-group">
              <label for="sapid">SAP ID</label>
              <input type="text" class="form-control" name="sapid" id="sapid" maxlength="18" value="<?php echo (isset($sapid) && !empty($sapid)) ? $sapid : ""; ?>">
              <small class="form-text text-muted"><?php if(isset($sapid_error) && !empty($sapid_error)){ echo $sapid_error; } ?></small>
            </div> <!--/.form-group-->
            <div class="form-group">
              <label for="hostname">Hostname</label>
              <input type="text" class="form-control" name="hostname" id="hostname" maxlength="14" value="<?php echo (isset($hostname) && !empty($hostname)) ? $hostname : ""; ?>">
              <small class="form-text text-muted"><?php if(isset($hostname_error) && !empty($hostname_error)){ echo $hostname_error; } ?></small>
            </div> <!--/.form-group-->
            <div class="form-group">
              <label for="loopback">Loopback (IPV4)</label>
              <input type="text" class="form-control" name="loopback" id="loopback" maxlength="20" value="<?php echo (isset($loopback) && !empty($loopback)) ? $loopback : ""; ?>">
              <small class="form-text text-muted"><?php if(isset($loopback_error) && !empty($loopback_error)){ echo $loopback_error; } ?></small>
            </div> <!--/.form-group-->
            <div class="form-group">
              <label for="mac_address">MAC Address</label>
              <input type="text" class="form-control" name="mac_address" id="mac_address" maxlength="30" value="<?php echo (isset($mac_address) && !empty($mac_address)) ? $mac_address : ""; ?>">
              <small class="form-text text-muted"><?php if(isset($mac_address_error) && !empty($mac_address_error)){ echo $mac_address_error; } ?></small>
            </div> <!--/.form-group-->
            <div class="form-check">
              <input type="checkbox" class="form-check-input" name="record_status" id="record_status" value="1" <?php echo ((isset($record_status) && ($record_status == 1)) || !isset($record_status)) ? "checked":""; ?>>
              <label class="form-check-label" for="record_status">Active</label>
            </div> <!--/.form-check-->
            <div class="row">
              <div class="col-md-6">
                <button type="button" class="btn btn-danger" name="btn_back" id="btn_back">Back</button>
              </div> <!--/.col-md6-->
              <div class="col-md-6 text-right">
                <button type="submit" class="btn btn-primary" name="btn_submit" id="btn_submit" value="submit">Submit</button>
              </div> <!--/.col-md-6-->
            </div> <!--/.row-->
          </form> <!--/.form-->
        </div> <!--/.col-md-12-->
      </div> <!--/.row-->
      <div class="row">
        <div class="col-md-12">&nbsp;</div> <!--/.col-md-12-->
      </div> <!--/.row-->
    </div> <!--/.container-->

    <footer class="text-muted">
      <div class="container">
        <p class="float-right">&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
      </div>
    </footer>
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="https://getbootstrap.com/docs/4.5/dist/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#btn_back").on("click", function(){
          window.open("<?php echo base_url(); ?>index.php/exercise1", "_parent");
        });
      });
    </script>
  </body>
</html>

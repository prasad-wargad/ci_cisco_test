<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Router_details extends CI_Model {
  public function __construct(){
    parent::__construct();
  }

  public function get_router_details($inputData = array()){
    $output_arr = array('data' => array());
    extract($inputData);
    $where_conditions = '';
    $where_condition_values = array();
    if(isset($record_id) && !empty($record_id) && is_numeric($record_id)){
      $where_conditions .= ' AND `id` = ?';
      $where_condition_values[] = $record_id;
    }
    $retrieved_data = $this->db->query('SELECT * FROM `router_details` WHERE 1'. $where_conditions.' ORDER BY `id` ASC;', $where_condition_values);
    if(is_array($retrieved_data->result_array()) && count($retrieved_data->result_array()) > 0){
      $output_arr['data'] = $retrieved_data->result_array();
    }
    unset($retrieved_data);
    return $output_arr;
  }

  public function router_list($inputData = array()) {
    $draw = 0;
    if(isset($inputData['draw']) && !empty($inputData['draw']) && $inputData['draw'] != '') {
      $draw = trim($inputData['draw']);
    }

    $start = 0;
    if(isset($inputData['start']) && !empty($inputData['start']) && $inputData['start'] != '') {
      $start = trim($inputData['start']);
    }

    $length = 0;
    if(isset($inputData['length']) && !empty($inputData['length']) && $inputData['length'] != '') {
      $length = trim($inputData['length']);
    }

    $tempColumns = array();   //Will try to retrieve data from POSTED Variables
    if(isset($inputData['columns']) && count($inputData['columns']) > 0) {
      $tempColumns=$inputData['columns'];
    }

    $strQuery = '';       //Will have the query
    $tempFieldList = '';
    $tempSearchFlds = '';

    if(is_array($tempColumns) && count($tempColumns) > 0) {
      foreach($tempColumns as $tempField) {
        switch(strtolower($tempField['data'])){
          case 'action':
            break;
          default:
            if(strtolower($tempField['data']) == 'created'){
              $tempFieldList .= " DATE_FORMAT(`created`,'%d %M %Y') AS `created`, ";
            }
            elseif(strtolower($tempField['data']) == 'status'){
              $tempFieldList .= " CASE WHEN(`status` = 1) THEN 'Active' ELSE 'Deleted' END AS `status`, ";
            }
            else{
              $tempFieldList .= "`". $tempField['data'] ."`, ";
            }

            $tempSearchValue = $tempField['search']['value'];
            if($tempSearchValue != ''){
              $tempSearchValue = trim(strip_tags($tempSearchValue));
              if(strtolower($tempField['data']) == 'created'){
                $tempSearchFromDate = '';
                $tempSearchToDate = '';
                $tempSearchValue = explode(',', $tempSearchValue);
                if( count($tempSearchValue) > 0 && 
                  isset($tempSearchValue[0]) && !empty($tempSearchValue[0]) ) {
                  $tempSearchFromDate  = $tempSearchValue[0];
                }

                if( !isset($tempSearchFromDate) || empty($tempSearchFromDate) || $tempSearchFromDate == '' ) {
                  $tempSearchFromDate = '1900/01/01';
                }
                if( strtolower($tempField['data']) == 'created' ) {
                  $tempSearchFlds .= ' AND `created` >= \''. $this->db->escape_str(str_replace("/", "-", $tempSearchFromDate)) .' 00:00:00\' ';
                }

                if( count($tempSearchValue) > 1 && 
                  isset($tempSearchValue[1]) && !empty($tempSearchValue[1]) ) {
                  $tempSearchToDate = $tempSearchValue[1];
                }

                if( isset($tempSearchToDate) && !empty($tempSearchToDate) && $tempSearchToDate != '' ) {
                  if( strtolower($tempField['data']) == 'created' ) {
                    $tempSearchFlds .= ' AND `created` <= \''. $this->db->escape_str(str_replace("/", "-", $tempSearchToDate)) .' 23:59:59\' ';
                  }
                }
              }
              else{
                $tempSearchFlds.=' AND (`'. $tempField['data'] .'` LIKE \'%'. $this->db->escape_str($tempSearchValue) .'%\') ';
              }
            }
            unset($tempSearchValue);
        }
      }
      unset($tempField);
    }

    $tempFieldList = trim($tempFieldList,', ');

    $tempOrderArr = array();
    if( isset($inputData['order']) && count($inputData['order']) > 0 ) {
      $tempOrderArr = $inputData['order'];
    }

    $tempOrderList = '';
    if( is_array($tempOrderArr) && count($tempOrderArr) > 0 ) {
      foreach( $tempOrderArr as $tempField ) {
        if( count($tempColumns) >= $tempField['column'] ) {
          switch($tempColumns[$tempField['column']]['data']) {
            case "Action":
              break;
            default:
                $tempOrderList.=' `'. $tempColumns[$tempField['column']]['data'] .'` '. strtoupper($tempField['dir']) .', ';
          }
        }
      }
      unset($tempField);
    }
    $tempOrderList=trim($tempOrderList,", ");
    if( isset($tempOrderList) && !empty($tempOrderList) && $tempOrderList != '' ) {
      $tempOrderList='ORDER BY '. $tempOrderList .' ';
    }

    $strQuery = 'SELECT SQL_CALC_FOUND_ROWS `id` AS `sr_id`, '. $tempFieldList .' '.
          'FROM `router_details` '.
          'WHERE 1 '. $tempSearchFlds .
          $tempOrderList .'LIMIT '. $start .','. $length .';';

    $noOfRecords=0;
    $retrieve_view_details = array();
    if( isset($strQuery) && !empty($strQuery) ) {
      $arrResultSet = $this->db->query($strQuery);
      if( is_array($arrResultSet->result_array()) && count($arrResultSet->result_array()) > 0 ) {
        $retrieve_view_details = $arrResultSet->result_array();
      }
      
      $arrResultSet = $this->db->query('SELECT FOUND_ROWS() AS `exp1`;');
      if( is_array($arrResultSet->result_array()) && count($arrResultSet->result_array()) > 0 ) {
        $arrResultSet = $arrResultSet->result_array();
        if( isset($arrResultSet[0]['exp1']) && !empty($arrResultSet[0]['exp1']) ) {
          $noOfRecords = $arrResultSet[0]['exp1'];
        }
      }
      unset($arrResultSet);
    }
    unset($strQuery);

    $data=array();
    foreach($retrieve_view_details as $tempField) {
      $strHtml  = "<a href='javascript:void(0);' class='is_edit' onClick='is_edit(". $tempField['sr_id'] .");'>Edit</a><br/>";
      if($tempField['status'] == 'Active'){
        $strHtml .= "<a href='javascript:void(0);' class='is_delete' onClick='is_delete(". $tempField['sr_id'] .");'>Delete</a>";
      }
      
      $tempDataArray=array();
      $tempDataArray=array_merge($tempDataArray,array("DT_RowId"=>base64_encode($tempField["sr_id"])));
      foreach( $tempField as $tempKeyField => $tempValueField ) {
        switch($tempKeyField) {
          case "sr_id":
            break;
          default:
            $tempDataArray=array_merge($tempDataArray,array($tempKeyField=>$tempValueField));
        }
      }
      $tempDataArray=array_merge($tempDataArray,array("action"=>$strHtml));
      if( isset($tempValueField) ) {
        unset($tempValueField);
      }

      if( isset($tempKeyField) ) {
        unset($tempKeyField);
      }
      $data[]=$tempDataArray;
    }
    unset($tempField, $retrieve_view_details);
    return array("draw"=>$draw, "recordsFiltered"=>$noOfRecords, "recordsTotal"=> $noOfRecords, "data"=>$data);
  }

  public function add_edit_router_details($inputData) {
    $record_id = 0;
    //Code to capture existing serial id coming from view page
    if( (isset($inputData['data']['id']) && !empty($inputData['data']['id']) && $inputData['data']['id']!="") )
    {  $record_id = $inputData['data']['id'];  }
    if( isset($record_id) && !empty($record_id) && $record_id!="" )
    {
      if( !is_numeric($record_id) )
      {  $record_id = 0;  }
    }
    unset($inputData['data']['id']);

    if($record_id > 0) {
      // update query
      $this->db->where('id', $record_id);
      unset($inputData['data']['id']);
      $inputData['data'] = $inputData['data'];
      return $this->db->update('router_details', $inputData['data']);
    }
    else {
      // insert query
      $inputData['data'] = $inputData['data'];
      $this->db->insert('router_details', $inputData['data']);
      return $this->db->insert_id();
    }
  }
}
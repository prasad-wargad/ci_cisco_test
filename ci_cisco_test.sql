-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2020 at 02:36 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_cisco_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `router_details`
--

CREATE TABLE `router_details` (
  `id` int(11) NOT NULL COMMENT 'Serial ID',
  `sapid` varchar(18) DEFAULT NULL COMMENT 'SAP ID',
  `hostname` varchar(14) DEFAULT NULL COMMENT 'Hostname',
  `loopback` varchar(20) DEFAULT NULL COMMENT 'IP V4',
  `mac_address` varchar(30) DEFAULT NULL COMMENT 'MAC Address',
  `status` tinyint(1) DEFAULT '1' COMMENT 'Status: 1 = Active, 0 = Deleted',
  `created` datetime DEFAULT NULL COMMENT 'Created Datetime',
  `modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Modified Datetime'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `router_details`
--

INSERT INTO `router_details` (`id`, `sapid`, `hostname`, `loopback`, `mac_address`, `status`, `created`, `modified`) VALUES
(1, 'aswer', 'asd', 'asd', 'asd', 0, '2020-06-29 13:50:54', '2020-06-29 17:20:54'),
(2, 'aswerasdasd', 'sdsdf', 'asd', 'asd', 1, '2020-06-29 13:49:16', '2020-06-29 17:19:16'),
(3, 'sapid_5ef9d85ecbaf', 'hostname_5ef9d', 'loopback_5ef9d85ecba', 'mac_address_5ef9d85ecbaf1', 1, '2020-06-29 14:02:38', NULL),
(4, 'sapid_5ef9d85ee06f', 'hostname_5ef9d', 'loopback_5ef9d85ee06', 'mac_address_5ef9d85ee06fe', 1, '2020-06-29 14:02:38', NULL),
(5, 'sapid_5ef9d85f0533', 'hostname_5ef9d', 'loopback_5ef9d85f053', 'mac_address_5ef9d85f05334', 1, '2020-06-29 14:02:39', NULL),
(6, 'sapid_5ef9d85f1223', 'hostname_5ef9d', 'loopback_5ef9d85f122', 'mac_address_5ef9d85f1223f', 1, '2020-06-29 14:02:39', NULL),
(7, 'sapid_5ef9d85f227f', 'hostname_5ef9d', 'loopback_5ef9d85f227', 'mac_address_5ef9d85f227fb', 1, '2020-06-29 14:02:39', NULL),
(8, 'sapid_5ef9d85f2d3d', 'hostname_5ef9d', 'loopback_5ef9d85f2d3', 'mac_address_5ef9d85f2d3dd', 1, '2020-06-29 14:02:39', NULL),
(9, 'sapid_5ef9d85f3d99', 'hostname_5ef9d', 'loopback_5ef9d85f3d9', 'mac_address_5ef9d85f3d999', 1, '2020-06-29 14:02:39', NULL),
(10, 'sapid_5ef9d85f53d1', 'hostname_5ef9d', 'loopback_5ef9d85f53d', 'mac_address_5ef9d85f53d16', 1, '2020-06-29 14:02:39', NULL),
(11, 'sapid_5ef9d85f60c2', 'hostname_5ef9d', 'loopback_5ef9d85f60c', 'mac_address_5ef9d85f60c21', 1, '2020-06-29 14:02:39', NULL),
(12, 'sapid_5ef9d85f6bbe', 'hostname_5ef9d', 'loopback_5ef9d85f6bb', 'mac_address_5ef9d85f6bbec', 1, '2020-06-29 14:02:39', NULL),
(13, 'sapid_5ef9d85f7bdb', 'hostname_5ef9d', 'loopback_5ef9d85f7bd', 'mac_address_5ef9d85f7bdbf', 1, '2020-06-29 14:02:39', NULL),
(14, 'sapid_5ef9d85f86d8', 'hostname_5ef9d', 'loopback_5ef9d85f86d', 'mac_address_5ef9d85f86d8a', 1, '2020-06-29 14:02:39', NULL),
(15, 'sapid_5ef9d85f9196', 'hostname_5ef9d', 'loopback_5ef9d85f919', 'mac_address_5ef9d85f9196d', 1, '2020-06-29 14:02:39', NULL),
(16, 'sapid_5ef9d85f9fc0', 'hostname_5ef9d', 'loopback_5ef9d85f9fc', 'mac_address_5ef9d85f9fc00', 1, '2020-06-29 14:02:39', NULL),
(17, 'sapid_5ef9d85facb0', 'hostname_5ef9d', 'loopback_5ef9d85facb', 'mac_address_5ef9d85facb0b', 1, '2020-06-29 14:02:39', NULL),
(18, 'asdasd', 'sdfdf', 'aswerwer', 'qwerqwerqwe', 1, '2020-06-29 14:35:40', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `router_details`
--
ALTER TABLE `router_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `router_details`
--
ALTER TABLE `router_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Serial ID', AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
